# Qualidade de Software  

## AGILE TESTING - A PRACTICAL GUIDE FOR TESTERS AND AGILE TEAMS  

![book](../images/agile-testing.jpg)


No ano de 2008, Lisa Crispin e Janet Gregory, publicaram a primeira versão de *AGILE TESTING - A PRACTICAL GUIDE FOR TESTERS AND AGILE TEAMS*. Desde então, esse livro se tornou uma referência para o universo de QAs e Testers que se inserem no contexto do desenvolvimento ágil. No capítulo II do livro, as autoras definiram __10 Pricípios e Valores para Testers em times Ágeis__.  




 __10 Pricípios e Valores para Testers em times Ágeis__

1. Fornecer feedback contínuo;
2. Entregar valores para o cliente;
3. Permitir comunicação/interação face a face;
4. Ter coragem;
5. Manter as coisas simples;
6. Praticar melhoria contínua;
7. Responder às mudanças;
8. Auto organizar-se;
9. Focar nas pessoas;
10. Encontrar maneiras de desfrutar/aproveitar no trabalho.  

