# Testar Apis com Postman   

1. Ler a documentação da API;
2. Criar workspace;
3. Convidar membros (caso necessário);
4. Criar ambientes e variáveis;
5. Criar estrutura de pastas da coleção;  
6. Criar requisições;
7. Parametizar variáveis;  
8. Criar arquivos de dados;
9. Paramentizar autenticação;
10. Criar pre requests scripts;
11. Criar testes;
12. Criar e executar com monitor(um monitor para cada ambiente);
13. Exportar resultados das execuções;  
14. Exportar coleção e ambiente;  
15. Instalar: nodeJS, newman, html-reporter e htmlextra;
16. Criar estrutura de pastas para Newman;
17. Executar via Newman coleção e ambientes exportados.  

Fonte:[Curso Postman + Projeto de Teste](https://www.udemy.com/course/automacao-de-testes-de-api-com-postman-projeto-de-testes/)  

## O que precisa ser testado em APIS?  

Varia muito com o contexto, porém, um teste básico valida os seguintes pontos:  

* Tempo de execução da request;  
* Status code esperado para requisição;  
* Conteúdo esperado no Body  

Cada um destes items deve ser validado dentro do objetivo de uma API. Por exemplo, numa situação de cadastro temos algumas operaçõe, como por exemplo criar um usuário, ler usuários, atualizar o cadastro, deletar o usário. Via API, para cada funcionalidade, devemos contemplar os requisitos acima. Há outras situações, como por exemplo segurança de dados, para este caso devemos verificar se os dados estão mascarados e não explícitos na resposta da api.
