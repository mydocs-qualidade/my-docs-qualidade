### Protocolo HTTP

A sigla HTTP signifca *HyperText Transfer Protocol*. A tradução literal é: Protocolo de Transferência de HiperTexto.  É o protocolo responsável pela troca de dados e conteúdo na web. O seu ponto de partida, é o momento que solicitamos algum tipo de conteúdo em alguma página, por exemplo, quando clicamos no botão de login. A partir disso, uma requisição é feita para o servidor. Neste momento, podemos dizer que o protocolo http estabelece uma relação entre quem faz a requisição (cliente) com quem processa a requisição e envia as informações (servidor). Por isso, o protocolo http é um protocolo cliente-servidor e base da troco de conteúdo na web.


* O HTTP fica na camada de aplicação e é o navegador que irá cuidar da sua implementação.  

* O protocolo é composto apenas de texto.

* A porta 80 é a porta padrão do HTTP

As requisições HTTP são organizadas por tipos, chamados de Verbos HTTP, e os pricipais tipos são os que veremos na seção abaixo.  

### __Verbos HTTP__

* __GET__ - Usado apenas para receber informações;
* __POST__ - Enviar informações novas para serem adcionadas ao recurso da URL;
* __PUT__ - Deve ser utilizado para atualizar recursos de uma determinada URL;
* __DELETE__ - Deve ser utilizado para pedir ao servidor que exclua o recurso de uma determinada URL.

Podemos ver os métodos HTTP e como funciona a requisição no DevTools do Browser.  

Para cada requisição, o servidor envia uma resposta sobre a situação da requisição, conhecidos como Status da requisição. Abaixo, apresentaremos os principais status.

### __Os Status das requisições__


* __1xx__  - Informativo de que a requisição foi recebida e processo continua;

*  __2xx__ - Sucesso, ocorreu tudo certo com a requisição;  
  * __200__: __OK__, usada quando a requisição foi processada e entregue com sucesso.  
  * __201__:  __CREATE__, Qunado a requisição cria um objeto e quer avisar ao cliente que a criação foi executada com sucesso.
  * __202__:  __ACCEPTED__,utilizada quando algo assícrono for executado. O servidor retorna que a requisição foi aceita e depois avisa quando tudo estiver concluído.

* __3xx__ - Redirecionamento, alguma ação precisará ser tomada para completar a requisição.
  * __301__ - __Moved Permanently__  - usado quando uma aplicação mudor de URL e não vai mais voltar para onde estava. Um do casos da sua utilização é quando um site muda de nome e um endereço antigo é mantido para redirecionar para o novo.
  * __304__ - __Not Modified__ - Utilizado para __cache__. Quando o arquivo não foi modificado o servidor retorna com esse status.

* __4xx__ - Erro no cliente. Possivelmente a requisição foi mal feita.
  * __403__ - __Forbidden__ - Quando o usuário que está fazendo a requisição não tem autorização para acessar o recurso.
  * __404__ - __Not Found__ - Usado quando o recurso que está sendo requisitado não foi encontrado.

* __5xx__ - Erro no servidor. A requisição parece válida, mas o servidor não consegue processar.
  * __500__ - __Internal Server Error__ - Utilizado quando o servidor não conseguiu processar a requisição por causa que algum erro interno.
  * __502__ - __Bad Gateway__ - Utilizado pelo servidor que está servindo como proxy. Ou seja, que está servindo as requisições para um ou mais servidores abaixo dele. Caso ele não receba uma resposta que seja válida do servidro que está abaixo dele, ele retorna o status 502;   

  Saiba mais em: [Uma visão geral do HTTP](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Overview)